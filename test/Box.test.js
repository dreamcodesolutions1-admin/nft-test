// Load dependencies
const { expect } = require('chai');

// Import utilities from Test Helpers
const { accounts, contract } = require('@openzeppelin/test-environment');

// Load compiled artifacts
const Box = contract.fromArtifact('Box');

// Start test block
describe('Box', function () {
    const [owner] = accounts;

    before(async function () {
      this.contract = await Box.new({from: owner});
    });
  
    // Test case
    it('retrieve returns a value previously stored', async function () {
        await this.contract.store(42, { from: owner });

        // Use large integer comparisons
        expect((await this.contract.retrieve()).toString()).to.equal('42');
    });

    /*it('store emits an event', async function () {
        const receipt = await this.contract.store(42, { from: owner });
    
        // Test that a ValueChanged event was emitted with the new value
        expectEvent(receipt, 'ValueChanged', { value: value });
    });

    it('non owner cannot store a value', async function () {
        // Test a transaction reverts
        await expectRevert(
            this.box.store(value, { from: other }),
            'Ownable: caller is not the owner',
        );
    });*/
  });